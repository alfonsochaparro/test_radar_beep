package io.alfcoding.radarbeeptest.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Calendar;

import io.alfcoding.radarbeeptest.R;
import io.alfcoding.radarbeeptest.models.Card;
import io.alfcoding.radarbeeptest.models.Game;
import io.alfcoding.radarbeeptest.utils.StringUtil;

/**
 * Created by Alfonso on 02/03/2017.
 */

@SuppressWarnings("WrongThread")
public class GameFragment extends Fragment {
    private Context mContext;
    private ProgressDialog mDialog;
    private TimerTask mTimer;

    // Models
    private int mCardsCount;
    private Game mGame;

    // Views
    private TextView mTimeView;
    private TextView mPointsView;
    private TextView mClicksView;
    private LinearLayout mCardsLayout;
    private View mLastCardTurned;

    private boolean mClicksEnabled = true;

    public static GameFragment newIntance(Game game) {
        GameFragment fragment = new GameFragment();
        fragment.mGame = game;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                Bundle savedInstanceState) {
        mContext = getActivity();

        View v = inflater.inflate(R.layout.fragment_game, container, false);
        initViews(v);
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();

        if(mTimer == null) {
            mTimer = new TimerTask();
            mTimer.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
        else {
            mTimer.setPaused(false);
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        if(mTimer != null) {
            mTimer.setPaused(true);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(mTimer != null && mTimer.getStatus() == AsyncTask.Status.RUNNING) {
            mTimer.cancel(true);
        }
    }

    //region Views methods

    private void initViews(View v) {
        mTimeView = (TextView) v.findViewById(R.id.txtTime);
        mPointsView = (TextView) v.findViewById(R.id.txtPoints);
        mClicksView = (TextView) v.findViewById(R.id.txtClicks);

        mCardsLayout = (LinearLayout) v.findViewById(R.id.layoutCards);

        generateCardsGrid();
    }

    private void generateCardsGrid() {
        int cardsCount = mGame.cards.size();
        int rows = cardsCount / 2;
        int cols = 2;

        boolean bestRatioFound = false;
        while(!bestRatioFound) {
            if(rows / 2 > cols) {
                rows = rows / 2;
                cols += 2;
            }
            else {
                bestRatioFound = true;
            }
        }

        mCardsLayout.setWeightSum(cols);
        LayoutInflater inflater = LayoutInflater.from(mContext);
        for(int i = 0; i < cols; i++) {
            inflater.inflate(R.layout.layout_cards_row, mCardsLayout);
            LinearLayout layout = (LinearLayout) mCardsLayout.getChildAt(mCardsLayout.getChildCount() - 1);
            layout.setWeightSum(rows);

            for(int j = 0; j < rows; j++) {
                int index = (i * rows) + j;
                inflater.inflate(R.layout.layout_card, layout);
                final View v = layout.getChildAt(layout.getChildCount() - 1);
                Card card = mGame.cards.get(index);
                v.setTag(card);

                v.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        v.animate().alpha(1f).scaleX(1f).scaleY(1f).setDuration(250);
                    }
                }, 50 * index);

                v.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(mClicksEnabled) {
                            turnCard(view);
                        }
                    }
                });
            }
        }
    }

    //endregion

    //region Game logic methods

    private void turnCard(View v) {
        v.animate().scaleX(0).setDuration(150);
        mClicksEnabled = false;

        new AsyncTask<View, Void, Integer>() {
            private Card mCard;
            private View mView;

            private boolean mGameCompleted;

            @Override
            protected Integer doInBackground(View... views) {
                mView = views[0];
                mCard = (Card) mView.getTag();
                try {
                    Thread.sleep(500);
                } catch (Exception e) { }

                int result = mGame.turnCard(mCard.id);
                mGameCompleted = mGame.isComplete();

                return result;
            }
            @Override
            protected void onPostExecute(Integer arg0) {
                final View innerCard = ((ViewGroup) mView).getChildAt(0);

                switch (arg0) {
                    case Game.ACTION_NOTHING:
                        break;
                    case Game.ACTION_SINGLE_TURN:
                        innerCard.setBackgroundResource(mCard.isUp ? mCard.drawable : R.drawable.card);
                        mLastCardTurned = mView;
                        break;
                    case Game.ACTION_PAIR_NOT_FOUND:
                        innerCard.setBackgroundResource(mCard.drawable);
                        innerCard.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                mView.animate().scaleX(0).setDuration(150);
                                mView.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        innerCard.setBackgroundResource(R.drawable.card);
                                        mView.animate().scaleX(1f).setDuration(150);
                                    }
                                }, 500);

                                if(mLastCardTurned != null) {
                                    mLastCardTurned.animate().scaleX(0).setDuration(150);
                                    final View lastInnerCard = ((ViewGroup) mLastCardTurned).getChildAt(0);
                                    mView.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            lastInnerCard.setBackgroundResource(R.drawable.card);
                                            mLastCardTurned.animate().scaleX(1f).setDuration(150);
                                            mLastCardTurned = null;
                                        }
                                    }, 500);
                                }
                            }
                        }, 1000);
                        break;
                    case Game.ACTION_PAIR_FOUND:
                        innerCard.setBackgroundResource(mCard.drawable);
                        mPointsView.setText(String.valueOf(mGame.points));
                        break;
                }

                mView.animate().scaleX(1f).setDuration(150);

                if(mGameCompleted) {
                    mTimer.cancel(true);
                    new AlertDialog.Builder(mContext)
                            .setTitle(getString(R.string.dialog_end_game_title))
                            .setMessage(getString(R.string.dialog_end_game_msg,
                                    String.valueOf(mGame.cards.size()),
                                    mTimeView.getText().toString().trim(),
                                    mClicksView.getText().toString().trim()))
                            .setPositiveButton(R.string.btn_ok, null)
                            .show();
                }
                else {
                    mClicksEnabled = true;
                }

                mClicksView.setText(String.valueOf(mGame.clicks));
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, v);
    }

    //endregion

    class TimerTask extends AsyncTask<Void, String, Void> {

        private Calendar mCalendar;
        private boolean mPaused = false;

        @Override
        protected Void doInBackground(Void... voids) {
            mCalendar = Calendar.getInstance();
            mCalendar.set(Calendar.MINUTE, 0);
            mCalendar.set(Calendar.SECOND, 0);
            while(!isCancelled()) {
                if(!mPaused) {
                    publishProgress(StringUtil.formatTime(mCalendar.getTime()));
                }
                try {
                    Thread.sleep(1000);
                    mCalendar.add(Calendar.SECOND, 1);
                } catch (InterruptedException e) {
                    // Fragment has been destroyed. Nothing to show
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(String... params) {
            mTimeView.setText(params[0]);
        }

        public void setPaused(boolean paused) {
            mPaused = paused;
        }
    }

}
