package io.alfcoding.radarbeeptest;

import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import io.alfcoding.radarbeeptest.fragments.GameFragment;
import io.alfcoding.radarbeeptest.models.Game;
import io.alfcoding.radarbeeptest.utils.Dialogs;

public class GameActivity extends AppCompatActivity {

    private View mWelcomeLayout;
    private ProgressDialog mDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        initViews();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.cards, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.menu_new_game) {
            newGameDialog();
        }

        return true;
    }

    private void initViews() {
        mWelcomeLayout = findViewById(R.id.layoutWelcome);

        findViewById(R.id.layoutImg).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                newGameDialog();
            }
        });

        mDialog = new ProgressDialog(this);
        mDialog.setMessage(getString(R.string.label_loading));
    }

    private void newGameDialog() {
        Dialogs.newGameDialog(this, new Dialogs.OnNewGameDoneListener() {
            @Override
            public void onNewGameDone(int cardsCount) {
                newGame(cardsCount);
            }
        });
    }

    private void newGame(int cardsCount) {
        new AsyncTask<Integer, Void, Game>() {
            private boolean mInvalidCardNumber = false;
            @Override
            protected void onPreExecute() {
                mDialog.show();
            }

            @Override
            protected Game doInBackground(Integer... params) {
                try {
                    return new Game(params[0]);
                } catch (Game.CardsNumberNotEven e) {
                    e.printStackTrace();
                    mInvalidCardNumber = true;
                    return null;
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }

            @Override
            protected void onPostExecute(Game arg0) {
                mDialog.dismiss();

                if(arg0 != null) {
                    createGameFragment(arg0);
                }
                else if(mInvalidCardNumber) {
                    Toast.makeText(getApplicationContext(), R.string.error_cards_numer,
                            Toast.LENGTH_SHORT).show();
                    newGameDialog();
                }
                else {
                    Toast.makeText(getApplicationContext(), R.string.error_unexpected,
                            Toast.LENGTH_SHORT).show();
                }
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, cardsCount);
    }

    private void createGameFragment(Game game) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, GameFragment.newIntance(game))
                .commit();

        if(mWelcomeLayout.getVisibility() == View.VISIBLE) {
            mWelcomeLayout.setVisibility(View.GONE);
        }
    }
}
