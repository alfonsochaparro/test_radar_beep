package io.alfcoding.radarbeeptest.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import io.alfcoding.radarbeeptest.R;

/**
 * Created by Alfonso on 02/03/2017.
 */

public class Dialogs {

    public static void newGameDialog(Context context, final OnNewGameDoneListener listener) {
        View v = LayoutInflater.from(context).inflate(R.layout.dialog_new_game, null);
        final EditText txt = (EditText) v.findViewById(R.id.txtCardsNumber);

        new AlertDialog.Builder(context)
                .setView(v)
                .setPositiveButton(R.string.btn_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        try {
                            listener.onNewGameDone(Integer.parseInt(txt.getText().toString().trim()));
                        } catch (Exception e) {
                            listener.onNewGameDone(0);
                        }
                    }
                })
                .setNegativeButton(R.string.btn_cancel, null)
                .show();
    }

    public interface OnNewGameDoneListener {
        public void onNewGameDone(int cardsCount);
    }
}
