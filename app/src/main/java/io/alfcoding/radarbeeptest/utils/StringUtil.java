package io.alfcoding.radarbeeptest.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Alfonso on 02/03/2017.
 */

public class StringUtil {

    private static final DateFormat sTimeFormat = new SimpleDateFormat("mm:ss");

    public static String formatTime(Date date) {
        return date != null ? sTimeFormat.format(date) : "";
    }
}
