package io.alfcoding.radarbeeptest.models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.alfcoding.radarbeeptest.R;

/**
 * Created by Alfonso on 02/03/2017.
 */

public class Game {

    public static int[] PALLETTE = new int[] {
            R.drawable.card_variant_1, R.drawable.card_variant_2, R.drawable.card_variant_3,
            R.drawable.card_variant_4, R.drawable.card_variant_5, R.drawable.card_variant_6
    };

    public static final int ACTION_SINGLE_TURN = 0;
    public static final int ACTION_PAIR_FOUND = 1;
    public static final int ACTION_PAIR_NOT_FOUND = 2;
    public static final int ACTION_NOTHING = 3;

    public int points;
    public int clicks;
    public List<Card> cards;

    public Game(int cardCount) throws CardsNumberNotEven {
        if(cardCount % 2 != 0) {
            throw new CardsNumberNotEven();
        }

        points = 0;
        cards = new ArrayList<>();

        int currentColor = 0;
        long id = System.currentTimeMillis();
        for(int i = 0; i < cardCount / 2; i++) {
            int drawable = PALLETTE[currentColor];
            currentColor = currentColor < PALLETTE.length - 1 ? currentColor + 1 : 0;

            cards.add(new Card(id++, drawable, false));
            cards.add(new Card(id++, drawable, false));
        }

        Collections.shuffle(cards);
    }

    public int turnCard(long id) {
        clicks++;
        for(Card card: cards) {
            if(card.id == id) {

                // If the card is already paired, won't do anything with it
                if(card.pairedWith == 0) {
                    card.isUp = !card.isUp;

                    if(card.isUp) {
                        // Checking if there is another card turned up without pairing
                        for (Card otherCard : cards) {
                            if (otherCard.id != card.id && otherCard.isUp
                                    && otherCard.pairedWith == 0) {

                                // Checking if the two cards are equals
                                if (otherCard.equals(card)) {
                                    card.pairedWith = otherCard.id;
                                    otherCard.pairedWith = card.id;
                                    points++;
                                    return ACTION_PAIR_FOUND;
                                }
                                else {
                                    // Cards were different! Turning them down back
                                    card.isUp = false;
                                    otherCard.isUp = false;
                                    return ACTION_PAIR_NOT_FOUND;
                                }
                            }
                        }
                    }
                    // Any other card was up, single turning done
                    return ACTION_SINGLE_TURN;
                }
                // Card was already paired, nothing done
                return ACTION_NOTHING;
            }
        }
        // Card not found in the cards array, nothing done
        return ACTION_NOTHING;
    }

    public boolean isComplete() {
        // Game is complete if all cards are paired

        for (Card card: cards) {
            if(card.pairedWith == 0) {
                return false;
            }
        }
        return true;
    }

    public class CardsNumberNotEven extends Exception {}
}
