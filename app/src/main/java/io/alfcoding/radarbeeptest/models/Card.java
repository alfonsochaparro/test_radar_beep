package io.alfcoding.radarbeeptest.models;

/**
 * Created by Alfonso on 02/03/2017.
 */

public class Card {

    public long id;
    public int drawable;
    public boolean isUp;
    public long pairedWith;

    public Card(long id, int drawable, boolean isUp) {
        this.id = id;
        this.drawable = drawable;
        this.isUp = isUp;

        this.pairedWith = 0;
    }

    @Override
    public boolean equals(Object o) {
        // Two cards are equals if they have the same color

        if(o == null) return false;
        if(!(o instanceof Card)) return false;
        return drawable == ((Card) o).drawable;
    }
}
