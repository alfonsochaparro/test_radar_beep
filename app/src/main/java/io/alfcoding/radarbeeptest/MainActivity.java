package io.alfcoding.radarbeeptest;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().hide();

        findViewById(R.id.imgLogo).postDelayed(new Runnable() {
            @Override
            public void run() {
                goAhead();
            }
        }, 2000);
    }

    private void goAhead() {
        startActivity(new Intent(this, GameActivity.class));
        finish();
    }
}
